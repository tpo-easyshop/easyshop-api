<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Store
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $admins
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $customers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Store whereUpdatedAt($value)
 */
class Store extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'city', 'pst',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function customers()
    {
        return $this->belongsToMany(User::class)->withPivot(['role_id'])->where('role_id', 1);
    }

    public function admins()
    {
        return $this->belongsToMany(User::class)->withPivot(['role_id'])->where('role_id', 3);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function addAdminUser(User $user)
    {
        $role_store_admin = Role::where('name', 'store_admin')->first();
        if (!$user->hasRole('store_admin')) {
            $user->roles()->attach($role_store_admin);
        }
        $user->stores()->attach($this, ['role_id' => $role_store_admin->id]);
    }

    public function removeAdminUser(User $user)
    {
        $role_store_admin = Role::where('name', 'store_admin')->first();
        $user->stores()->detach($this, ['role_id' => $role_store_admin->id]);

        if ($user->stores->count() == 0) {
            $user->roles()->detach($role_store_admin);
        }
    }
}
