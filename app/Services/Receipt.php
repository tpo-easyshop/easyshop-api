<?php

namespace App\Services;

use App\Product;
use App\Store;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;

class Receipt
{
    //inject PDF generator
    public function __construct()
    {

    }

    public function generate($eans, $storeId)
    {
        $quantity = array_count_values($eans);

        $products = Product::whereIn('ean', $eans)->get();

        foreach ($products as &$product) {
            $product['quantity'] = $quantity[$product['ean']];
        }

        $store = Store::whereId($storeId)->first();
        $pdf = App::make("dompdf.wrapper");
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->loadView('store/checkout/pdf_invoice', [
            "products" => $products,
            "store" => $store,
            "sum" => $this->calculateSum($products),
        ]);

        return $pdf->stream();
    }

    private function calculateSum($products)
    {
        $sum = ['no_vat' => 0, 'vat' => 0];

        foreach ($products as $product) {
            $sum['no_vat'] += $product['quantity'] * $product['price'] / ($product['vat'] / 100 + 1);
            $sum['vat'] += $product['quantity'] * $product['price'];
        }
        
        return $sum;
    }
}