<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Product;
use App\Services\Receipt;
use App\Store;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generateReceipt(Request $request, Receipt $receipt, $store_id)
    {
        $receipt = $receipt->generate($request->get("eans"), $store_id);

        return $receipt;
        return response()->json($receipt);
    }
}
