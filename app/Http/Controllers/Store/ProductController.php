<?php

namespace App\Http\Controllers\Store;

use App\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Product;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class ProductController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric'],
            'vat' => ['required', 'numeric'],
            'ean' => ['required', 'string'],
        ]);
    }

    public function index(Request $request, Store $store)
    {
        if (!$request->user()) {
            abort(403);
        }

        $request->user()->authorizeRoles(['admin', 'store_admin']);

        return view("store/product/products", [
            'products' => Product::all(),
            'store' => $store
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Store $store
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        return view("store/product/create_product", ['store'=>$store]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Store $store
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);
        $data = $request->all();
        $this->validator($data)->validate();

        $image = $request->file("image");
        $image->filename = sprintf("%s.%s", time(), $image->getClientOriginalExtension());
        $destinationPath = public_path('/images');

        $image->move($destinationPath, $image->filename);

        $data['image'] = $image->filename;

        $store->products()->save(new Product($data));

        return redirect(route('stores.products.index', ['store' => $store]))->with('success', 'Product has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Store $store
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Request $request, Store $store, Product $product)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        return view("store/product/edit_product", ['product' => $product, 'store' => $store]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Product $product
     * @param Store $store
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Store $store, Product $product)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        $this->validator($request->all())->validate();

        $product->update($request->all());

        return redirect(route('stores.products.index', ['store' => $store]))->with('success', 'Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Store $store
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Store $store, Product $product)
    {
        $product->delete();
        return redirect(route('stores.products.index', ['store' => $store]))->with('success', 'Product has been deleted!');
    }
}
