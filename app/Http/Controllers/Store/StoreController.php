<?php

namespace App\Http\Controllers\Store;

use App\Product;
use App\Store;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string'],
            'pst' => ['required', 'numeric', 'between:1000,9999'],
        ]);
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        if ($request->user()->hasRole('admin')) {
            $stores = Store::all();
        } else {
            $stores = $request->user()->stores;
        }

        return view('store/store/stores', ['stores' => $stores, 'user' => $request->user()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        return view("store/store/create_store");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        $data = $request->all();
        $this->validator($data)->validate();
        Store::create($data);


        return redirect(route('stores.index'))->with('success', 'Store has been created.');
    }

    public function edit(Request $request, $id)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        return view('store/store/edit_store', ['store' => Store::whereId($id)->first()]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        $this->validator($request->all())->validate();

        Store::whereId($id)->first()->update($request->all());

        return redirect(route('stores.index'))->with('success', 'Store has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $request->user()->authorizeRoles(['admin', 'store_admin']);

        Store::destroy($id);
        return redirect(route('stores.index'))->with('success', 'Store has been deleted!');
    }

    public function addRemoveAdmin(Request $request, $id)
    {
        $request->user()->authorizeRoles(['admin']);

        return view('store/store/add_remove_admin', ['store'=> Store::whereId($id)->first(), 'users' => User::all()]);
    }

    public function addAdmin(Request $request, Store $store, User $user)
    {
        $request->user()->authorizeRoles(['admin']);

        $store->addAdminUser($user);
        return redirect(route('stores.add_remove_admin', ['id' => $store['id']]))->with('success', 'Admin has been added.');
    }

    public function removeAdmin(Request $request, Store $store, User $user)
    {
        $request->user()->authorizeRoles(['admin']);

        $store->removeAdminUser($user);
        return redirect(route('stores.add_remove_admin', ['id' => $store['id']]))->with('success', 'Admin has been removed.');
    }

    public function products(Store $store)
    {
        return view("store/product/products", [
            'products' => $store->products,
            'store' => $store,
        ]);
    }
}
