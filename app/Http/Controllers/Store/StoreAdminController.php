<?php

namespace App\Http\Controllers\Store;

use App\Store;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        return view('store/admins/store_admins', ['stores' => Store::all()]);
    }
}
