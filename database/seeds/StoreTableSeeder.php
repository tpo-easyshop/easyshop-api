<?php

use App\Store;
use Illuminate\Database\Seeder;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Store();
        $user->name = 'Mercator';
        $user->address = 'Celovška cesta 163';
        $user->city = 'Ljubljana';
        $user->pst = 1000;
        $user->save();

        $user = new Store();
        $user->name = 'Lidl Bežigrad';
        $user->address = 'Bežigrad 11';
        $user->city = 'Ljubljana';
        $user->pst = 1000;
        $user->save();

        $user = new Store();
        $user->name = 'Hofer';
        $user->address = 'Celovška cesta 224';
        $user->city = 'Ljubljana';
        $user->pst = 1000;
        $user->save();

        $user = new Store();
        $user->name = 'Spar';
        $user->address = 'Čopova ulica 38';
        $user->city = 'Ljubljana';
        $user->pst = 1000;
        $user->save();
    }
}
