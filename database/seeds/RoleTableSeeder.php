<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'customer';
        $role->description = 'A customer';
        $role->save();

        $role = new Role();
        $role->name = 'admin';
        $role->description = 'A super admin';
        $role->save();

        $role = new Role();
        $role->name = 'store_admin';
        $role->description = 'A store admin';
        $role->save();
    }
}
