<?php

use App\Role;
use App\Store;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_customer = Role::where('name', 'customer')->first();
        $role_admin= Role::where('name', 'admin')->first();
        $role_store_admin = Role::where('name', 'store_admin')->first();

        $store1 = Store::where('id', 1)->first();

        $user = new User();
        $user->name = 'Jože Kupec';
        $user->email = 'customer@example.com';
        $user->password = bcrypt('pass');
        $user->save();
        $user->roles()->attach($role_customer);

        $user = new User();
        $user->name = 'Peter Admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('pass');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Marko Store Admin';
        $user->email = 'store.admin@example.com';
        $user->password = bcrypt('pass');
        $user->save();
        $user->roles()->attach($role_store_admin);
        $user->stores()->attach($store1, ['role_id' => $role_store_admin->id]);

    }
}
