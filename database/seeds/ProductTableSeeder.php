<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = \App\Store::whereId(1)->first();
        $product = new Product();
        $product->name = 'Trajno polnomastno Alpsko mleko, 3,5 % m.m., Ljubljanske mlekarne, 1 l';
        $product->price = 1.09;
        $product->vat = 8.5;
        $product->ean = "535776";
        $product->image = "00299496.jpg";
        $product->save();
        $store->products()->save($product);

        $product = new Product();
        $product->name = 'Keksi s pomarančnim polnilom, Jaffacakes, 300 g';
        $product->price = 1.99;
        $product->vat = 8.5;
        $product->ean = "299496";
        $product->image = "00306872.jpg";
        $product->save();
        $store->products()->save($product);

        $product = new Product();
        $product->name = 'Jajca M, hlevska reja, Mercator, 10 jajc';
        $product->price = 1.59;
        $product->vat = 8.5;
        $product->ean = "306872";
        $product->image = "1543690695.jpg";
        $product->save();
        $store->products()->save($product);
    }
}
