# stage 1
FROM composer:1.7.3 as build_stage1
WORKDIR /composer/
COPY composer.json ./
COPY composer.lock ./
RUN composer install --no-scripts --no-autoloader

# stage 2
FROM composer:1.7.3 as build_stage2
WORKDIR /composer/
COPY --from=build_stage1 /composer /composer/
COPY . /composer/
RUN composer dump-autoload --optimize

# final image
FROM php:7.2.12-apache-stretch

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load

RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini

COPY --from=build_stage2 /composer /var/www/html/
RUN chown -R www-data:www-data /var/www/html/
