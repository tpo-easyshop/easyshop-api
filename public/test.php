<?php

$html_head = "<html>
<head>
	<style type='text/css'>
		.table_items
		{
			border-collapse: collapse;
			text-align:left;
			width: 100%;
		}
        .table_items th
		{
			border-bottom: 1px solid black;
		}
		
		.table_price
		{
			border-collapse: collapse;
			text-align:left;
		}
        .table_price th
		{
			border-top: 1px solid black;
		}
    </style>
</head>

<body style='width:700px;'>

<div>
<p><h2>[Naziv_trgovine]</h2></p>
<p>[Naslov_trgovine]</p>
</div>
<hr/>

<br/>

<div>
<p><h1>Račun št.: [Stevilka_racuna]</h1></p>
<p>Datum računa: " . date('d/m/Y h:i:s a', time()) . "</p>
</div>

<br/>
<br/>
<br/>";

$html_items_start = "<table class='table_items'>
<tr>
	<th>Zap.</th>
	<th>Šifra</th>
	<th>Naziv</th>
	<th>Količina</th>
	<th>EM</th>
	<th>Cena</th>
	<th>Popust %</th>
	<th>DDV %</th>
	<th>Vrednost</th>
</tr>";

$html_item = "<tr>
	<td>[Zaporedna_st]</td>
	<td>[Id_izdelka]</td>
	<td>[Naziv]</td>
	<td>[Kolicina]</td>
	<td>Kos</td>
	<td>[Cena]</td>
	<td>[Popust]%</td>
	<td>22.5%</td>
	<td>[Vrednost]</td>
</tr>";

$html_items_end = "</table>
<br/>";

$html_prices = "<table class='table_price'>
<tr>
	<td>Znesek brez DDV:</td>
	<td>[Znesek_brez_DDV]</td>
</tr>
<tr>
	<td>Znesek DDV:</td>
	<td>[Znesek_DDV]</td>
</tr>
<tr>
	<th>SKUPAJ EUR:</th>
	<th>[Skupaj]</th>
</tr>

</table>


</body>
</html>";

$html_items = array();//array itemov

$product1 = new stdClass();
$product1->Name = "Mleko";
$product1->Id = 6231;
$product1->Price = 1.5;

$products = [$product1];

$ddv = 22.5;//koliko je DDV
$price_no_DDV = 0;
$price_DDV = 0;

$i = 1;

foreach ($products as $product)	//za vsak produkt dodaš vrstico
{
    $html_item_new = str_replace("[Zaporedna_st]", $i . ".", $html_item);
    $html_item_new = str_replace("[Id_izdelka]", $product->Id, $html_item_new);
    $html_item_new = str_replace("[Naziv]", $product->Name, $html_item_new);
    $html_item_new = str_replace("[Cena]", $product->Price . " €", $html_item_new);
    $html_item_new = str_replace("[Vrednost]", $product->Price * 0.225. " €", $html_item_new);
    array_push($html_items, $html_item_new);

    $price_no_DDV += $product->Price;	//računaš ceno
    $price_DDV += $product->Price * 0.225;

    $i++;
}

//dodaš še končno ceno
$html_prices = str_replace("[Znesek_brez_DDV]", $price_no_DDV, $html_prices);
$html_prices = str_replace("[Znesek_DDV]", $price_DDV . " €", $html_prices);
$html_prices = str_replace("[Skupaj]", $price_no_DDV + $price_DDV, $html_prices);

//vrneš združene stvari
echo $html_head . $html_items_start . $html_items[0] . $html_items_end . $html_prices;
?>