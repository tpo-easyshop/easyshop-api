<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/customers', 'HomeController@index')->name('customers');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/stores/{store}/products', 'Store\StoreController@products')->name('stores.products.index');
    Route::get('/stores/{store}/products/create', 'Store\ProductController@create')->name('stores.products.create');
    Route::post('/stores/{store}/products/store', 'Store\ProductController@store')->name('stores.products.store');
    Route::get('/stores/{store}/products/{product}', 'Store\ProductController@edit')->name('stores.products.edit');
    Route::put('/stores/{store}/products/{product}', 'Store\ProductController@update')->name('stores.products.update');
    Route::delete('/stores/{store}/products/{product}', 'Store\ProductController@destroy')->name('stores.products.destroy');

    Route::get('/stores/{id}/add_remove_admin', 'Store\StoreController@addRemoveAdmin')->name('stores.add_remove_admin');
    Route::post('/stores/{store}/add_admin/{user}', 'Store\StoreController@addAdmin')->name('stores.add_admin');
    Route::post('/stores/{store}/add_remove/{user}', 'Store\StoreController@removeAdmin')->name('stores.remove_admin');

//    Route::resource('products', 'Store\ProductController');
    Route::resource('stores', 'Store\StoreController');
});
//Route::get('/products', 'Store\ProductController@index')->name('products');
//Route::get('/product/{id}', 'Store\ProductController@edit')->name('edit_product');
//Route::get('/product/{id}/destroy', 'Store\ProductController@destroy')->name('destroy_product');
//Route::get('/product/{id}/create', 'Store\ProductController@destroy')->name('destroy_product');

