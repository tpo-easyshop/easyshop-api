<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\RegisterController@registerApi');
Route::post('login', 'Auth\LoginController@loginApi');
Route::post('logout', 'Auth\LoginController@logoutApi');

Route::group(['middleware' => 'auth:api'], function () {
//    Route::get('products', 'ProductController@test');
});

Route::get('products/{ean}', 'Api\ProductController@show');
Route::get('stores/{store_id}/products/{ean}', 'Api\ProductController@show');

Route::post('checkout', 'Api\CheckoutController@generateReceipt');
Route::post('stores/{store_id}/checkout', 'Api\CheckoutController@generateReceipt');
