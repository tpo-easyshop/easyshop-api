@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Stores</div>

                    <div class="card-body">

                        <h1>
                            List of stores
                            <a href="{{ route('products.create') }}" class="btn btn-primary float-right">Add new <i class="fas fa-plus"></i></a>
                        </h1>

                        <table class="table mt-4">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Ean</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $key => $product)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>{{ $product['name'] }}</td>
                                    <td>{{ $product['price'] }} €</td>
                                    <td>{{ $product['ean'] }}</td>
                                    <td class="text-right">
                                        <a class="ml-2 btn btn-primary" href="{{ route('products.edit', $product['id']) }}" title="edit"><i class="fas fa-edit"></i></a>
                                        <form class="delete-button" action="{{ route('products.destroy', $product['id'])}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit"><i class="fas fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
