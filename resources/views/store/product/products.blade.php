@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    <h1>
                        List of products [{{ $store['name'] }}]
                        <a href="{{ route('stores.products.create', ['store' => $store]) }}" class="btn btn-primary float-right">Add new <i class="fas fa-plus"></i></a>
                    </h1>

                    <table class="table mt-4">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Ean</th>
                            <th scope="col">Image</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $key => $product)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>{{ $product['name'] }}</td>
                                    <td>{{ $product['price'] }} €</td>
                                    <td>
                                        <svg class="barcode" id="barcode_{{ $product['id'] }}" ean="{{ $product['ean'] }}"></svg>
                                    </td>
                                    <td>
                                        @if (!empty($product['image']))
                                            <img src="{{ url('/images/' . $product['image']) }}" style="max-height: 150px" />
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a class="ml-2 btn btn-primary" href="{{ route('stores.products.edit', ['product' => $product['id'], 'store'=>$store]) }}" title="edit"><i class="fas fa-edit"></i></a>
                                        <form class="delete-button" action="{{ route('stores.products.destroy', ['product' => $product['id'], 'store'=>$store])}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit"><i class="fas fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
