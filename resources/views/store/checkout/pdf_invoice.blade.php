<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type='text/css'>
            body {
                font-family: DejaVu Sans;
            }
            .table_items {
                border-collapse: collapse;
                text-align: left;
                width: 100%;
            }

            .table_items th {
                border-bottom: 1px solid black;
                white-space: nowrap;
            }

            .table_price {
                border-collapse: collapse;
                text-align: left;
            }

            .table_price th {
                border-top: 1px solid black;
            }
        </style>
    </head>

    <body>

        <div>
            <h2>{{ $store['name'] }}</h2>
            <p>{{ $store['address'] }}, {{ $store['pst'] }} {{ $store['city'] }}</p>
        </div>
        <hr/>

        <br/>

        <div>
            <p>
            {{--<h1>Račun št.: [Stevilka_racuna]</h1></p>--}}
            <p>Datum računa: {{ date('d/m/Y H:i:s') }}</p>
        </div>

        <br/>
        <br/>
        <br/>

        <table class='table_items'>
            <tr>
                <th>Zap.</th>
                <th>Šifra</th>
                <th>Naziv</th>
                <th>Količina</th>
                <th>EM</th>
                <th>Cena</th>
                <th>Popust %</th>
                <th>DDV %</th>
                <th>Vrednost</th>
            </tr>

            @foreach ($products as $key => $product)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ $product['id'] }}</td>
                    <td>{{ $product['name'] }}</td>
                    <td>{{ $product['quantity'] }}</td>
                    <td>Kos</td>
                    <td>{{ $product['price'] }}</td>
                    <td></td>
                    <td>{{ $product['vat'] }}</td>
                    <td>{{ $product['price'] * $product['quantity'] }}</td>
                </tr>
            @endforeach
        </table>
        <br/>
        <table class='table_price'>
            <tr>
                <td>Znesek brez DDV:</td>
                <td>{{ number_format($sum['no_vat'], 2) }}</td>
            </tr>
            <tr>
                <td>Znesek DDV:</td>
                <td>{{ number_format($sum['vat'] - $sum['no_vat'], 2) }}</td>
            </tr>
            <tr>
                <th>SKUPAJ EUR:</th>
                <th>{{ number_format($sum['vat'], 2) }}</th>
            </tr>

        </table>
    </body>
</html>