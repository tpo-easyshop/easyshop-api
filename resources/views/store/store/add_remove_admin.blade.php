@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Add/remove store admin for ') }}[{{ $store['name'] }}]</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                            <ul>
                                @foreach ($users as $key => $user)
                                    <li class="mb-1">
                                        @if ($user->isStoreAdmin($store))
                                            <form class="d-inline-block" method="POST" action="{{ route('stores.remove_admin', ['id' => $store['id'], 'user_id' => $user['id']]) }}">
                                                @csrf
                                                <button class="mr-2 btn btn-danger"><i class="fas fa-minus"></i></button>
                                            </form>
                                        @else
                                            <form class="d-inline-block" method="POST" action="{{ route('stores.add_admin', ['id' => $store['id'], 'user_id' => $user['id']]) }}">
                                                @csrf
                                                <button class="mr-2 btn btn-success"><i class="fas fa-plus"></i></button>
                                            </form>
                                        @endif
                                        {{ $user['name'] }}
                                    </li>
                                @endforeach
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
