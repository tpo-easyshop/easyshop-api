@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Stores</div>

                    <div class="card-body">

                        <h1>
                            List of stores
                            @if ($user->hasRole('admin'))
                                <a href="{{ route('stores.create') }}" class="btn btn-primary float-right">Add new <i class="fas fa-plus"></i></a>
                            @endif
                        </h1>
                        <table class="table mt-4">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Store name</th>
                                @if ($user->hasRole('admin'))
                                    <th scope="col">Number of admins</th>
                                @endif
                                <th scope="col">Products</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($stores as $key => $store)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>{{ $store['name'] }}</td>

                                    @if ($user->hasRole('admin'))
                                        <td>
                                            {{ count($store->admins) }}
                                            <a class="ml-2 btn btn-primary" href="{{ route('stores.add_remove_admin', $store['id']) }}" title="Add or remove admins"><i class="fas fa-user"></i></a>
                                        </td>
                                    @endif
                                    <td>
                                        {{ $store->products->count() }}
                                        <a class="ml-2 btn btn-primary" href="{{ route('stores.products.index', $store['id']) }}" title="Edit products"><i class="fas fa-boxes"></i></a>
                                    </td>
                                    <td class="text-right">
                                        <a class="ml-2 btn btn-primary" href="{{ route('stores.edit', $store['id']) }}" title="edit"><i class="fas fa-edit"></i></a>
                                        <form class="delete-button" action="{{ route('stores.destroy', $store['id'])}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit"><i class="fas fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
